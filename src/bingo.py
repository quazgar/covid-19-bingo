#!/usr/bin/env python3

import argparse
import datetime
import random
import sys
import yaml

import numpy as np
import pandas as pd
from tabulate import tabulate
import IPython

CSV_URL = "https://pavelmayer.de/covid/risks/data.csv"
LIMIT_DEFAULT = 50

def _check_matrix(array):
    """Check if there is a bingo line in the boolean matrix.

    Returns
    -------
    out : bool
    True if a bingo line could be found, else False.
    """
    width = array.shape[0]
    for x in range(width):
        if array[x, :].all():
            return True
    for y in range(width):
        if array[:, y].all():
            return True
    if all([array[x, x] for x in range(width)]):
        return True
    if all([array[x, width-1-x] for x in range(width)]):
        return True
    return False

def _format_inz(inzidenz, limit):
    """Add nice decoration if inzidenz >= limit.  Return an appropriate string."""
    if inzidenz < limit:
        return str(int(inzidenz))
    return "** {} **".format(int(inzidenz))


def neu(args):
    """Erzeugt eine neue Bingo-Matrix."""
    table = pd.read_csv(CSV_URL)
    kreise = table.loc[(table['FaellePro100kLetzte7Tage'] < 35) &
                       ((table['LandkreisTyp'] == "LK") | (table['LandkreisTyp'] == "SK")) ]
    width = args.width
    size = width**2
    if len(kreise) < size:
        print("Zombie-Alarm! Es gibt nicht mehr genug Kreise mit Inzidenzen unter 35, um die "
              "Bingo-Tafel zu füllen.")
        sys.exit(2)
    kreis_namen = list(kreise.Landkreis)
    chosen = random.sample(kreis_namen, size)
    yaml.safe_dump(chosen, stream=args.dateiname)
    return table


def test(args, table=None):
    """Überprüft eine Bingo-Matrix."""
    # Get data
    if table is None:
        table = pd.read_csv(CSV_URL)
    kreise = yaml.safe_load(args.datei)
    size = len(kreise)
    width = round(float(np.sqrt(len(kreise))))  # https://github.com/numpy/numpy/issues/11810
    assert width**2 == size
    kreis_daten = table.set_index("Landkreis").loc[kreise]

    # Find matching lines
    inzidenzen = kreis_daten.FaellePro100kLetzte7Tage
    inzidenzen = np.array(inzidenzen).reshape([width, width])
    durchseucht = inzidenzen >= args.limit
    bingo = _check_matrix(durchseucht)

    # Pretty print result
    kreis_texte = ["{}\n{}".format(
        kreis[3:] if (kreis.startswith("LK ") or kreis.startswith("SK ")) else kreis,
        _format_inz(row.FaellePro100kLetzte7Tage, args.limit))
                   for kreis, row in kreis_daten.iterrows()]
    a_texte = np.array(kreis_texte).reshape([width, width])
    print(tabulate(a_texte, tablefmt="grid"))

    # Print winning state
    if bingo:
        print("""

Bingo!

Deine Bingo-Tafel ist erfolgreich durchinfiziert.  Weihnachten fällt dieses Jahr wohl aus.
""")
    else:
        print("""

Alles gut, noch besteht Hoffnung.
""")

    # Print as PDF?
    if args.print:
        pdf_filename = args.print
        _print(a_texte, pdf_filename)


def _print(array, pdf_filename):
    """Print the array into a figure file."""
    import matplotlib.pyplot as plt
    plt.figure(figsize=(6, 6))
    axis = plt.gca()
    plt_table = axis.table(cellText=array, loc="center")
    plt_table.scale(1, 5)
    axis.set_frame_on(False)
    axis.xaxis.set_visible(False)
    axis.yaxis.set_visible(False)
    axis.set_title(str(datetime.date.today()))
    plt.savefig(pdf_filename, pad_inches=0.5)

    # IPython.embed()


def main():
    """The main function."""
    args = parse_arguments()
    action = args.action
    if action == "neu":
        table = neu(args)
        args.dateiname.close()
        args.datei = open(args.dateiname.name, mode="r", encoding=args.dateiname.encoding)
        args.print = None
        args.limit = LIMIT_DEFAULT
        # args.limit = 20  # For debugging
        test(args, table=table)
        args.datei.close()
    elif action == "test":
        test(args)
        args.datei.close()
    else:
        raise NotImplementedError("Unbekannte Aktion: {}".format(action))


def parse_arguments():
    """Parse the arguments."""
    parser = argparse.ArgumentParser(
        description='Erzeugt oder überprüft eine Covid-19-Bingo-Tafel.')
    actions = parser.add_subparsers(help='Was soll gemacht werden?  Mehr Hilfe mit `neu -h` oder '
                                    '`test -h`.', required=True, dest="action")

    p_neu = actions.add_parser("neu", help="Eine neue Bingo-Tafel erzeugen.")
    p_test = actions.add_parser("test", help="Eine Bingo-Tafel überprüfen.")

    p_neu.add_argument('-f', '--dateiname', help="Dateiname, in den die neue Bingo-Tafel "
                       "gespeichert werden soll.  Standardmäßig `bingo_<DATUM>.c19b`.",
                       type=argparse.FileType('w'), default="bingo_{}.c19b".format(
                           datetime.date.today()))
    p_neu.add_argument('-n', '--breite', help="Wie breit und hoch soll die Bingo-Tafel sein?",
                       type=int, default=5, dest="width")


    p_test.add_argument('datei', help="Die Bingo-Datei, die überprüft werden soll.",
                        type=argparse.FileType('r'))
    p_test.add_argument('-l', '--limit', help='Ab welchem Inzidenzwert gilt ein Kreis als '
                        '"durchinfiziert"?.', type=int, default=50)
    p_test.add_argument('-p', '--print', help="Wohin (pdf-Dateiname) soll die Tafel als PDF "
                        "gespeichert werden?")

    return parser.parse_args()


if __name__ == '__main__':
    main()
